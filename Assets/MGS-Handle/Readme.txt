==========================================================================
  Copyright (C), 2016-2017, Mogoson tech. Co., Ltd.
  Name: MGS-Handle
  Author: Mogoson   Version: 1.0   Date: 4/1/2016
==========================================================================
  [Summeray]
    This package can be used to make button switch, knob switch and
    rocker handle in Unity3D scene.
--------------------------------------------------------------------------
  [Environment]
    Package applies to Unity3D 5.0, .Net Framework 3.0 or above version.
--------------------------------------------------------------------------
  [Usage]
    Find the demos in the path "MGS-Handle/Scenes".
    Understand the usages of component scripts in the demos.
    Use the compnent scripts in your project.
--------------------------------------------------------------------------
  [Contact]
    If you have any questions, feel free to contact me at mogoson@qq.com.
--------------------------------------------------------------------------